# CoreMetrics

---

[TOC]

## Overview

This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to __CoreMetrics__.

## How we send the data

Campaign data is being sent once per session. We use the function based on the `tag` selection when you are initializing the integration (see [Prerequisite](#prerequisite) section).

One of the following functions can be used:

+ `window.cmCreateElementTag()` _(element tag)_

+ `window.cmCreatePageviewTag()` _(pageview tag)_

+ `window.cmCreateProductviewTag()` _(productview tag)_

Campaign experience is passed to the function as an argument.

_Example:_

```javascript
window.cmCreatePageviewTag('MM_Prod_T1Button=color:red')
```

## Data Format

The data sent to CoreMetrics will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
)

## Download

* [coremetrics-register.js](https://bitbucket.org/mm-global-se/if-coremetrics/src/master/src/coremetrics-register.js)

* [coremetrics-initialize.js](https://bitbucket.org/mm-global-se/if-coremetrics/src/master/src/coremetrics-initialize.js)

## Prerequisite

The following information needs to be provided by the client: 

+ Campaign name

+ Tags
  
  _Supported tags:_ 

  * [Element tag](http://www-01.ibm.com/support/knowledgecenter/SSZLC2_7.0.0/com.ibm.commerce.Coremetrics.doc/refs/rmtuseelem.htm?lang=en)
  
  * [Page view tag](http://www-01.ibm.com/support/knowledgecenter/SSZLC2_7.0.0/com.ibm.commerce.Coremetrics.doc/refs/rmtusepgvw.htm?lang=en)
  
  * [product](http://www-01.ibm.com/support/knowledgecenter/SSZLC2_7.0.0/com.ibm.commerce.Coremetrics.doc/refs/rmtuseprdt.htm?lang=en)

## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [CoreMetrics-register.js](https://bitbucket.org/mm-global-se/if-coremetrics/src/master/src/coremetrics-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Google Analytics Register scripts!

+ Create a campaign script and add the [CoreMetrics-initialize.js](https://bitbucket.org/mm-global-se/if-coremetrics/src/master/src/coremetrics-initialize.js) script. Customise the code by changing the campaign name, account ID (only if needed), and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.

## QA

+ Add the [ObservePoint](https://chrome.google.com/webstore/detail/observepoint-tag-debugger/daejfbkjipkgidckemjjafiomfeabemo) plugin in your Google Chrome browser.

+ Navigate to the page with the integration and open your developer tools by pressing F12 on your keyboard. Then refresh the page.

+ Click the “ObservePoint” tab in your developer tools.

+ From the list of Tag Names, click the call to “IBM Coremetrics”. It should open a side-by-side window to show the data being transferred to Coremetrics on that call.

+ The Maxymiser data should be stored under the “Variables” tab. 

![coremetrics.png](https://bitbucket.org/repo/jyrq69/images/3969794125-coremetrics.png)